/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 15:29:31 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/28 16:03:18 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV_H
# define RTV_H

# include "libft.h"
# include <mlx.h>
# include <math.h>
# include <fcntl.h>
# include <time.h>

# define W 1000
# define H 1000
# define RGB 255
# define M 1e-4

typedef struct	s_mlx
{
	void		*init;
	void		*win;
	void		*image;
}				t_mlx;

/*
** Sphere - 1, Cylinder - 2
** Cone - 3, Plane - 4
*/

typedef struct	s_dcoef
{
	float	a;
	float	b;
	float	c;
	float	d;
}				t_dcoef;

typedef struct	s_xyz
{
	float		x;
	float		y;
	float		z;
}				t_xyz;

typedef	struct	s_ssphere
{
	t_xyz	*coor;
	float	r;
	float	angle;
	int		color;
}				t_ssphere;

typedef struct	s_clndr
{
	t_xyz	*coor;
	t_xyz	*dir;
	float	r;
	int		color;
}				t_clndr;

typedef struct	s_cone
{
	t_xyz	*coor;
	t_xyz	*dir;
	float	k;
	int		color;
}				t_cone;

typedef struct	s_ftr
{
	t_xyz	*coor;
	t_xyz	*dir;
	int		color;
}				t_ftr;

typedef struct	s_ray
{
	t_xyz		*raypos;
	t_xyz		*raydir;
}				t_ray;

typedef struct	s_read
{
	t_ssphere	**ssphere;
	t_clndr		**cyl;
	t_cone		**cone;
	t_ftr		**plane;
	t_xyz		*camcoor;
	t_xyz		*camdir;
	t_xyz		*light;
	t_mlx		*mlx;
	t_ray		*ray;
	int			figs;
	int			figflag;
	int			num_fig;
	t_ray		*vect;
	float		dot1;
	float		dot2;
	float		t;
	float		temp;
	int			flag;
	int			color;
	float		alpha;
	int			sphere_num;
	int			cone_num;
	int			plane_num;
	int			cyl_num;
	int			sphere_num2;
	int			cone_num2;
	int			plane_num2;
	int			cyl_num2;
	int			fig_flag;
	t_xyz		vec1;
}				t_read;

void			ft_save(char ***fig, t_read *read);
void			ft_read(char *argv, char ****fig);
void			ft_err(void);
int				ft_atoi_base(char *str, int base);
void			ft_plane_malloc(char ***fig, t_read *read, int c, int *i);
void			ft_cone_malloc(char ***fig, t_read *read, int c, int *i);
void			ft_cylinder_malloc(char ***fig, t_read *read, int c, int *i);
void			ft_sphere_malloc(char ***fig, t_read *read, int c, int *i);
void			ft_initial(t_read *read);
void			ft_save(char ***fig, t_read *read);
void			ft_ariadne(char ***fig, t_read *read, int c);
void			ft_camera_malloc(char ***fig, t_read *read, int c);
void			ft_light_malloc(char ***fig, t_read *read, int c);
void			ft_num_fig(char ***fig, t_read *read);
int				ft_count_w(char *argv);
void			ft_num(t_read *read, char **fig);
int				ft_str2len(char **str);
void			ft_color_init(t_read *read);
int				ft_rgba_to_int(float alpha, int r, int g, int b);
int				ft_check_rgb(int c);
void			ft_put_to_im(int x, int y, int rgb, t_mlx *mlx);
t_xyz			ft_mult(t_xyz *vec1, float t);
t_xyz			ft_sum(t_xyz *vec1, t_xyz *vec2);
t_xyz			ft_diff(t_xyz *vec1, t_xyz *vec2);
t_xyz			ft_normal(t_xyz *vec1);
float			ft_dot(t_xyz *v1, t_xyz *v2);
int				ft_check_sphere_inter(t_read *r, int i);
int				ft_check_plane_inter(t_read *r, int i);
int				ft_check_cyl_inter(t_read *r, int i);
int				ft_check_cone_inter(t_read *r, int i);
int				ft_check_sphere_inter2(t_read *r, int i, t_xyz *o, t_xyz *d);
int				ft_check_plane_inter2(t_read *r, int i, t_xyz *o, t_xyz *d);
int				ft_check_cyl_inter2(t_read *r, int i, t_xyz *o, t_xyz *d);
int				ft_check_cone_inter2(t_read *r, int i, t_xyz *o, t_xyz *d);
void			ft_plane_angle(t_read *rtv, int i);
void			ft_cone_angle(t_read *r, int i);
void			ft_cone_angle_helper(t_read *r, int i, t_xyz *normal);
void			ft_cylinder_angle(t_read *r, int i);
void			ft_cylinder_angle_helper(t_read *r, int i, t_xyz *normal);
void			ft_sphere_angle(t_read *r, int i);
void			ft_intersect_true(t_read *read);
void			ft_set_intersect_angle(t_read *read, int flag, int max);
void			ft_light_intersect(t_read *read, int flag, int max, float dist);
void			ft_cam_intersect(t_read *read, int flag, int max);
void			ft_set_intersect_flag(t_read *r, int fl, int i);
void			ft_intersect_all(t_read *read);
void			ft_rtv(t_read *read);

#endif
