# **RTv1** #
## RTv1 is a program developped for my learning course for the exploration of raytracing.
## Several options are available
* 	Select objects within the scene
* 	Translate and rotate the camera
* 	Translate and rotate the selected object
* 	Increase the radius/angle of the selected object
* 	Increase the maximum number of reflections
* 	Activate reflections on planes only
*	Increase the anti-aliasing
* 	Hide or show the HUD
* 	Activate gloss
## The program can support multiple lights and objects in a scene. The scene and objects parameters can be modified in real-time. This software also uses multitheading to fasten up the calculations. All memory allocated during execution is freed. 
## **Install & launch** 
* git clone https://nmatushe@bitbucket.org/nmatushe/rtv.git
* cd ~/rtv && ./rtv scenes/scene1.rt

You have to launch the program with a parameter. This is the name of the scene you would like to open at the execution of the program. The input file has to follow a predefined layout. A parser implemented in the program will check for layout errors.

Example : ⇣

./rtv scenes/scene5.rt



