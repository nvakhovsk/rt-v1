/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color_image.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 15:15:47 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/28 15:17:23 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void	ft_put_to_im(int x, int y, int rgb, t_mlx *mlx)
{
	int				bitspp;
	int				slen;
	int				en;
	char			*image;
	unsigned int	tmp;

	image = mlx_get_data_addr(mlx->image, &bitspp, &slen, &en);
	tmp = (mlx_get_color_value(mlx->init, rgb));
	if (y > 0 && y < H && x > 0 && x < W)
		ft_memcpy((void *)(image + slen * y + x *
			sizeof(int)), (void *)&tmp, 4);
}

int		ft_check_rgb(int c)
{
	if (c < 0)
		c = 0;
	else if (c > RGB)
		c = RGB;
	return (c);
}

int		ft_rgba_to_int(float alpha, int r, int g, int b)
{
	r = (r + RGB * alpha) / 2;
	r = ft_check_rgb(r);
	g = ((g + RGB * alpha) / 2);
	g = ft_check_rgb(g);
	b = ((b + RGB * alpha) / 2);
	b = ft_check_rgb(b);
	return (r << 16 | g << 8 | b);
}

void	ft_color_init(t_read *read)
{
	(read->flag == 1) ?
			read->color = ft_rgba_to_int(read->alpha, 100, 100, 100) : 0;
	(read->flag == 2) ?
			read->color = ft_rgba_to_int(read->alpha, 0, 100, 200) : 0;
	(read->flag == 3) ?
			read->color = ft_rgba_to_int(read->alpha, 100, 0, 0) : 0;
	(read->flag == 4) ?
			read->color = ft_rgba_to_int(read->alpha, 158, 40, 99) : 0;
	(read->flag == 66) ?
			read->color = ft_rgba_to_int(0, 0, 0, 0) : 0;
}
