/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intersection.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 15:30:43 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/28 16:03:10 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void	ft_set_intersect_flag(t_read *r, int fl, int i)
{
	r->fig_flag = i;
	if (r->temp > M)
	{
		if (r->temp > r->t)
		{
			r->temp = r->t;
			r->flag = fl;
		}
	}
	else
	{
		r->temp = r->t;
		r->flag = fl;
	}
}

void	ft_cam_intersect(t_read *read, int flag, int max)
{
	int	i;

	i = -1;
	while (++i < max)
	{
		if (flag == 1 && ft_check_sphere_inter(read, i))
			ft_set_intersect_flag(read, flag, i);
		if (flag == 2 && ft_check_cyl_inter(read, i))
			ft_set_intersect_flag(read, flag, i);
		if (flag == 3 && ft_check_cone_inter(read, i))
			ft_set_intersect_flag(read, flag, i);
		if (flag == 4 && ft_check_plane_inter(read, i))
			ft_set_intersect_flag(read, flag, i);
	}
}

void	ft_light_intersect(t_read *read, int flag, int max, float dist)
{
	int	i;

	if (read->flag != flag)
	{
		i = -1;
		while (++i < max)
		{
			if (flag == 1 && ft_check_sphere_inter2(read, i,
						read->vect->raypos, read->vect->raydir))
				if (read->t > 0 && read->t < dist)
					read->flag = 66;
			if (flag == 2 && ft_check_cyl_inter2(read, i,
						read->vect->raypos, read->vect->raydir))
				if (read->t > 0 && read->t < dist)
					read->flag = 66;
			if (flag == 3 && ft_check_cone_inter2(read, i,
						read->vect->raypos, read->vect->raydir))
				if (read->t > 0 && read->t < dist)
					read->flag = 66;
			if (flag == 4 && ft_check_plane_inter2(read, i,
						read->vect->raypos, read->vect->raydir))
				if (read->t > 0 && read->t < dist)
					read->flag = 66;
		}
	}
}

void	ft_set_intersect_angle(t_read *read, int flag, int max)
{
	int	i;

	i = -1;
	while (++i < max && flag == 1)
		read->flag == flag ? ft_sphere_angle(read, i) : 0;
	i = -1;
	while (++i < max && flag == 2)
		read->flag == flag ? ft_cylinder_angle(read, i) : 0;
	i = -1;
	while (++i < max && flag == 3)
		read->flag == flag ? ft_cone_angle(read, i) : 0;
	i = -1;
	while (++i < max && flag == 4)
		read->flag == flag ? ft_plane_angle(read, i) : 0;
}

void	ft_intersect_true(t_read *read)
{
	t_xyz	tmp;
	float	dist;

	read->vec1 = ft_mult(read->camdir, read->temp);
	read->vec1 = ft_sum(read->camcoor, &read->vec1);
	tmp = ft_diff(read->light, &read->vec1);
	dist = sqrt(ft_dot(&tmp, &tmp));
	tmp = ft_normal(&tmp);
	read->vect->raydir = &tmp;
	read->vect->raypos = &read->vec1;
	ft_light_intersect(read, 1, read->sphere_num, dist);
	ft_light_intersect(read, 2, read->cyl_num, dist);
	ft_light_intersect(read, 3, read->cone_num, dist);
	ft_light_intersect(read, 4, read->plane_num, dist);
	ft_set_intersect_angle(read, 1, read->sphere_num);
	ft_set_intersect_angle(read, 2, read->cyl_num);
	ft_set_intersect_angle(read, 3, read->cone_num);
	ft_set_intersect_angle(read, 4, read->plane_num);
}
