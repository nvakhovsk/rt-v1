/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_oper.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 15:18:30 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/28 15:21:03 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

float	ft_dot(t_xyz *v1, t_xyz *v2)
{
	return ((float)(v1->x * v2->x + v1->y * v2->y + v1->z * v2->z));
}

t_xyz	ft_normal(t_xyz *vec1)
{
	t_xyz	a;
	float	m;

	m = sqrt(ft_dot(vec1, vec1));
	a.x = vec1->x / m;
	a.y = vec1->y / m;
	a.z = vec1->z / m;
	return (a);
}

t_xyz	ft_diff(t_xyz *vec1, t_xyz *vec2)
{
	t_xyz a;

	a.x = vec1->x - vec2->x;
	a.y = vec1->y - vec2->y;
	a.z = vec1->z - vec2->z;
	return (a);
}

t_xyz	ft_sum(t_xyz *vec1, t_xyz *vec2)
{
	t_xyz a;

	a.x = vec1->x + vec2->x;
	a.y = vec1->y + vec2->y;
	a.z = vec1->z + vec2->z;
	return (a);
}

t_xyz	ft_mult(t_xyz *vec1, float t)
{
	t_xyz a;

	a.x = vec1->x * t;
	a.y = vec1->y * t;
	a.z = vec1->z * t;
	return (a);
}
