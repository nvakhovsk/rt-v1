/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_figure_initial.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 15:08:07 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/28 15:09:52 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void	ft_initial(t_read *read)
{
	read->camcoor = (t_xyz *)malloc(sizeof(t_xyz));
	read->camdir = (t_xyz *)malloc(sizeof(t_xyz));
	read->ssphere = (t_ssphere **)malloc(sizeof(t_ssphere *) *
				read->sphere_num);
	read->cone = (t_cone **)malloc(sizeof(t_cone *) * read->cone_num);
	read->plane = (t_ftr **)malloc(sizeof(t_ftr *) * read->plane_num);
	read->cyl = (t_clndr **)malloc(sizeof(t_clndr *) * read->cyl_num);
}

void	ft_sphere_malloc(char ***fig, t_read *read, int c, int *i)
{
	if (ft_str2len(fig[c]) != 5)
		ft_err();
	read->ssphere[++read->sphere_num2] =
				(t_ssphere *)malloc(sizeof(t_ssphere));
	read->ssphere[read->sphere_num2]->coor = (t_xyz *)malloc(sizeof(t_xyz));
	read->ssphere[read->sphere_num2]->coor->x = ft_atoi(fig[c][1]);
	read->ssphere[read->sphere_num2]->coor->y = ft_atoi(fig[c][2]);
	read->ssphere[read->sphere_num2]->coor->z = ft_atoi(fig[c][3]);
	read->ssphere[read->sphere_num2]->r = ft_atoi(fig[c][4]);
	*i += 1;
}

void	ft_cylinder_malloc(char ***fig, t_read *read, int c, int *i)
{
	if (ft_str2len(fig[c]) != 8)
		ft_err();
	read->cyl[++read->cyl_num2] = (t_clndr *)malloc(sizeof(t_clndr));
	read->cyl[read->cyl_num2]->coor = (t_xyz *)malloc(sizeof(t_xyz));
	read->cyl[read->cyl_num2]->dir = (t_xyz *)malloc(sizeof(t_xyz));
	read->cyl[read->cyl_num2]->coor->x = ft_atoi(fig[c][1]);
	read->cyl[read->cyl_num2]->coor->y = ft_atoi(fig[c][2]);
	read->cyl[read->cyl_num2]->coor->z = ft_atoi(fig[c][3]);
	read->cyl[read->cyl_num2]->dir->x = ft_atoi(fig[c][4]);
	read->cyl[read->cyl_num2]->dir->y = ft_atoi(fig[c][5]);
	read->cyl[read->cyl_num2]->dir->z = ft_atoi(fig[c][6]);
	read->cyl[read->cyl_num2]->r = ft_atoi(fig[c][7]);
	*i += 1;
}

void	ft_cone_malloc(char ***fig, t_read *read, int c, int *i)
{
	if (ft_str2len(fig[c]) != 8)
		ft_err();
	read->cone[++read->cone_num2] = (t_cone *)malloc(sizeof(t_cone));
	read->cone[read->cone_num2]->coor = (t_xyz *)malloc(sizeof(t_xyz));
	read->cone[read->cone_num2]->dir = (t_xyz *)malloc(sizeof(t_xyz));
	read->cone[read->cone_num2]->coor->x = ft_atoi(fig[c][1]);
	read->cone[read->cone_num2]->coor->y = ft_atoi(fig[c][2]);
	read->cone[read->cone_num2]->coor->z = ft_atoi(fig[c][3]);
	read->cone[read->cone_num2]->dir->x = ft_atoi(fig[c][4]);
	read->cone[read->cone_num2]->dir->y = ft_atoi(fig[c][5]);
	read->cone[read->cone_num2]->dir->z = ft_atoi(fig[c][6]);
	read->cone[read->cone_num2]->k = ft_atoi(fig[c][7]);
	*i += 1;
}

void	ft_plane_malloc(char ***fig, t_read *read, int c, int *i)
{
	if (ft_str2len(fig[c]) != 7)
		ft_err();
	read->plane[++read->plane_num2] = (t_ftr *)malloc(sizeof(t_ftr));
	read->plane[read->plane_num2]->coor = (t_xyz *)malloc(sizeof(t_xyz));
	read->plane[read->plane_num2]->dir = (t_xyz *)malloc(sizeof(t_xyz));
	read->plane[read->plane_num2]->coor->x = ft_atoi(fig[c][1]);
	read->plane[read->plane_num2]->coor->y = ft_atoi(fig[c][2]);
	read->plane[read->plane_num2]->coor->z = ft_atoi(fig[c][3]);
	read->plane[read->plane_num2]->dir->x = ft_atoi(fig[c][4]);
	read->plane[read->plane_num2]->dir->y = ft_atoi(fig[c][5]);
	read->plane[read->plane_num2]->dir->z = ft_atoi(fig[c][6]);
	*i += 1;
}
