/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_writing.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 15:11:43 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/28 16:03:15 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void	ft_num_fig(char ***fig, t_read *read)
{
	int	i;
	int	j;

	read->sphere_num = 0;
	read->cone_num = 0;
	read->plane_num = 0;
	read->cyl_num = 0;
	i = -1;
	j = 0;
	while (fig[++i] != NULL)
	{
		ft_strequ(fig[i][0], "sphere") == 1 ? read->sphere_num++ : 0;
		ft_strequ(fig[i][0], "cone") == 1 ? read->cone_num++ : 0;
		ft_strequ(fig[i][0], "plane") == 1 ? read->plane_num++ : 0;
		ft_strequ(fig[i][0], "cylinder") == 1 ? read->cyl_num++ : 0;
	}
	read->sphere_num2 = -1;
	read->cone_num2 = -1;
	read->plane_num2 = -1;
	read->cyl_num2 = -1;
}

void	ft_light_malloc(char ***fig, t_read *read, int c)
{
	if (ft_str2len(fig[c]) != 4)
		ft_err();
	read->light->x = ft_atoi(fig[c][1]);
	read->light->y = ft_atoi(fig[c][2]);
	read->light->z = ft_atoi(fig[c][3]);
}

void	ft_camera_malloc(char ***fig, t_read *read, int c)
{
	if (ft_str2len(fig[c]) != 7)
		ft_err();
	read->camcoor->x = ft_atoi(fig[c][1]);
	read->camcoor->y = ft_atoi(fig[c][2]);
	read->camcoor->z = ft_atoi(fig[c][3]);
	read->camdir->x = ft_atoi(fig[c][4]);
	read->camdir->y = ft_atoi(fig[c][5]);
	read->camdir->z = ft_atoi(fig[c][6]);
}

void	ft_ariadne(char ***fig, t_read *read, int c)
{
	if (ft_strequ(fig[c][0], "light") == 1)
		ft_light_malloc(fig, read, c);
	else if (ft_strequ(fig[c][0], "camera") == 1)
		ft_camera_malloc(fig, read, c);
}

void	ft_save(char ***fig, t_read *read)
{
	int	i;
	int	c;

	ft_num_fig(fig, read);
	ft_initial(read);
	i = 0;
	c = -1;
	while (fig[++c])
	{
		if (ft_strequ(fig[c][0], "sphere") == 1 &&
					read->sphere_num2 <= read->sphere_num)
			ft_sphere_malloc(fig, read, c, &i);
		else if (ft_strequ(fig[c][0], "cylinder") == 1 &&
					read->cyl_num2 <= read->cyl_num)
			ft_cylinder_malloc(fig, read, c, &i);
		else if (ft_strequ(fig[c][0], "cone") == 1 &&
					read->cone_num2 <= read->cone_num)
			ft_cone_malloc(fig, read, c, &i);
		else if (ft_strequ(fig[c][0], "plane") == 1 &&
					read->plane_num2 <= read->plane_num)
			ft_plane_malloc(fig, read, c, &i);
		else
			ft_ariadne(fig, read, c);
	}
	read->num_fig = i;
}
