/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_light_inter.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 15:25:28 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/28 15:26:05 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

int		ft_check_sphere_inter2(t_read *r, int i, t_xyz *o, t_xyz *d)
{
	t_dcoef	dcoef;
	t_xyz	vec;

	vec = ft_diff(o, r->ssphere[i]->coor);
	dcoef.a = ft_dot(d, d);
	dcoef.b = 2 * ft_dot(d, &vec);
	dcoef.c = ft_dot(&vec, &vec) - pow(r->ssphere[i]->r, 2);
	dcoef.d = pow(dcoef.b, 2) - 4 * dcoef.a * dcoef.c;
	if (dcoef.d < 0)
		return (0);
	else
	{
		r->dot1 = (-dcoef.b + sqrt(dcoef.d)) / (2 * dcoef.a);
		r->dot2 = (-dcoef.b - sqrt(dcoef.d)) / (2 * dcoef.a);
		r->t = (r->dot1 <= r->dot2) ? r->dot1 : r->dot2;
	}
	return (1);
}

int		ft_check_plane_inter2(t_read *r, int i, t_xyz *o, t_xyz *d)
{
	float	t1;
	float	t2;
	t_xyz	normal;
	t_xyz	t3;

	normal = ft_normal(r->plane[i]->dir);
	t3 = ft_diff(r->plane[i]->coor, o);
	t1 = ft_dot(&t3, &normal);
	t2 = ft_dot(d, &normal);
	if (t2 == 0)
		return (0);
	if (t1 / t2 < 0)
		return (0);
	r->t = t1 / t2;
	return (1);
}

int		ft_check_cyl_inter2(t_read *r, int i, t_xyz *o, t_xyz *d)
{
	t_dcoef	dcoef;
	t_xyz	vec;
	t_xyz	dir_normal;

	dir_normal = ft_normal(r->cyl[i]->dir);
	vec = ft_diff(o, r->cyl[i]->coor);
	dcoef.a = ft_dot(d, d) - pow(ft_dot(d, &dir_normal), 2);
	dcoef.b = 2 * (ft_dot(d, &vec) -
		(ft_dot(d, &dir_normal) * ft_dot(&vec, &dir_normal)));
	dcoef.c = ft_dot(&vec, &vec) -
		pow(ft_dot(&vec, &dir_normal), 2) - pow(r->cyl[i]->r, 2);
	dcoef.d = pow(dcoef.b, 2) - 4 * dcoef.a * dcoef.c;
	if (dcoef.d < 0)
		return (0);
	else
	{
		r->dot1 = (-dcoef.b + sqrt(dcoef.d)) / (2 * dcoef.a);
		r->dot2 = (-dcoef.b - sqrt(dcoef.d)) / (2 * dcoef.a);
		r->t = (r->dot1 <= r->dot2 && r->dot1 > 0) ? r->dot1 : r->dot2;
	}
	return (1);
}

int		ft_check_cone_inter2(t_read *r, int i, t_xyz *o, t_xyz *d)
{
	t_dcoef	dcoef;
	float	alpha;
	t_xyz	vec;
	t_xyz	dir_normal;

	dir_normal = ft_normal(r->cone[i]->dir);
	alpha = 1 + pow(tan(r->cone[i]->k * M_PI / 180), 2);
	vec = ft_diff(o, r->cone[i]->coor);
	dcoef.a = ft_dot(d, d) - alpha * pow(ft_dot(d, &dir_normal), 2);
	dcoef.b = 2 * (ft_dot(d, &vec) - alpha *
		ft_dot(d, &dir_normal) * ft_dot(&vec, &dir_normal));
	dcoef.c = ft_dot(&vec, &vec) - alpha * pow(ft_dot(&vec, &dir_normal), 2);
	dcoef.d = pow(dcoef.b, 2) - 4 * dcoef.a * dcoef.c;
	if (dcoef.d < 0)
		return (0);
	else
	{
		r->dot1 = (-dcoef.b + sqrt(dcoef.d)) / (2 * dcoef.a);
		r->dot2 = (-dcoef.b - sqrt(dcoef.d)) / (2 * dcoef.a);
		r->t = (r->dot1 <= r->dot2 && r->dot1 > 0) ? r->dot1 : r->dot2;
	}
	return (1);
}
