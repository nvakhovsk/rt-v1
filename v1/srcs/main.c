/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 11:53:13 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/28 15:33:10 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

int		ft_exit(void *t)
{
	if (!t)
		exit(0);
	return (0);
}

int		ft_key(int key)
{
	(key == 53) ? exit(0) : 0;
	return (0);
}

void	ft_intersect_all(t_read *read)
{
	read->t = 0;
	read->temp = 0;
	read->alpha = 0;
	read->flag = 0;
	ft_cam_intersect(read, 1, read->sphere_num);
	ft_cam_intersect(read, 2, read->cyl_num);
	ft_cam_intersect(read, 3, read->cone_num);
	ft_cam_intersect(read, 4, read->plane_num);
	if (read->flag)
		ft_intersect_true(read);
}

void	ft_rtv(t_read *read)
{
	int		x;
	int		y;

	y = -1;
	while (++y < W)
	{
		x = -1;
		while (++x < H)
		{
			read->camdir->x = (2 * ((x + 0.5 + read->camdir->x) / W) - 1) *
								tan(30 * M_PI / 180);
			read->camdir->y = (1 - 2 * ((y + 0.5 + read->camdir->y) / H)) *
								tan(30 * M_PI / 180);
			read->color = ft_rgba_to_int(0, 255, 255, 255);
			ft_intersect_all(read);
			ft_color_init(read);
			ft_put_to_im(x, y, read->color, read->mlx);
		}
	}
}

int		main(int argc, char **argv)
{
	char	***fig;
	t_read	*read;

	if (argc != 2)
		return (0);
	read = (t_read *)malloc(sizeof(t_read));
	read->mlx = (t_mlx *)malloc(sizeof(t_mlx));
	read->mlx->init = mlx_init();
	read->mlx->image = mlx_new_image(read->mlx->init, W, H);
	read->mlx->win = mlx_new_window(read->mlx->init, W, H, "rtv1");
	read->vect = (t_ray *)malloc(sizeof(t_ray));
	read->light = (t_xyz *)malloc(sizeof(t_xyz));
	ft_read(argv[1], &fig);
	ft_save(fig, read);
	ft_rtv(read);
	mlx_hook(read->mlx->win, 17, 0L, ft_exit, NULL);
	mlx_hook(read->mlx->win, 2, 1, ft_key, NULL);
	mlx_put_image_to_window(read->mlx->init, read->mlx->win,
		read->mlx->image, 0, 0);
	mlx_loop(read->mlx->init);
}
