/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reading.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/09 10:10:00 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/28 15:15:02 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void			ft_err(void)
{
	ft_putendl("error :)");
	exit(0);
}

void			ft_num(t_read *read, char **fig)
{
	read->sphere_num = ft_atoi(fig[1]);
	read->cone_num = ft_atoi(fig[2]);
	read->plane_num = ft_atoi(fig[3]);
	read->cyl_num = ft_atoi(fig[4]);
}

int				ft_str2len(char **str)
{
	int	i;

	i = -1;
	while (str[++i])
		;
	return (i);
}

int				ft_count_w(char *argv)
{
	int		w;
	int		fd;
	char	*tmp;

	w = -1;
	(fd = open(argv, O_RDONLY)) < 0 ? ft_err() : 0;
	while (get_next_line(fd, &tmp))
	{
		++w;
		free(tmp);
	}
	close(fd);
	return (w);
}

void			ft_read(char *argv, char ****fig)
{
	int		fd;
	int		i;
	char	**map;
	int		w;

	w = ft_count_w(argv) + 1;
	i = -1;
	(fd = open(argv, O_RDONLY)) < 0 ? ft_err() : 0;
	map = (char **)malloc(sizeof(char *) * (w + 1));
	while (++i < w)
		get_next_line(fd, &(map)[i]);
	close(fd);
	(map)[i++] = NULL;
	i = -1;
	*fig = (char ***)malloc(sizeof(char **) * (w + 1));
	while (++i < w)
		(*fig)[i] = ft_strsplit((map)[i], ' ');
	(*fig)[i] = NULL;
	i = -1;
	while (++i < w)
		free((map)[i]);
	free(map);
}
