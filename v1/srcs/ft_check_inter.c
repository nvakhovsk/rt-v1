/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_inter.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 15:22:54 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/28 15:34:01 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void	ft_plane_angle(t_read *rtv, int i)
{
	t_xyz	normal;
	t_xyz	normal_light;
	t_xyz	dir_normal;
	float	temp_a;

	dir_normal = ft_normal(rtv->plane[i]->dir);
	normal_light = ft_diff(rtv->light, &rtv->vec1);
	normal_light = ft_normal(&normal_light);
	normal = ft_normal(rtv->plane[i]->dir);
	temp_a = ft_dot(&normal_light, &normal);
	if (rtv->alpha < temp_a)
		rtv->alpha = temp_a;
}

int		ft_check_sphere_inter(t_read *r, int i)
{
	t_dcoef	dcoef;
	t_xyz	vec;

	vec = ft_diff(r->camcoor, r->ssphere[i]->coor);
	dcoef.a = ft_dot(r->camdir, r->camdir);
	dcoef.b = 2 * ft_dot(r->camdir, &vec);
	dcoef.c = ft_dot(&vec, &vec) - pow(r->ssphere[i]->r, 2);
	dcoef.d = pow(dcoef.b, 2) - 4 * dcoef.a * dcoef.c;
	if (dcoef.d < 0)
		return (0);
	else
	{
		r->dot1 = (-dcoef.b + sqrt(dcoef.d)) / (2 * dcoef.a);
		r->dot2 = (-dcoef.b - sqrt(dcoef.d)) / (2 * dcoef.a);
		r->t = (r->dot1 <= r->dot2) ? r->dot1 : r->dot2;
	}
	return (1);
}

int		ft_check_plane_inter(t_read *r, int i)
{
	float	t1;
	float	t2;
	t_xyz	normal;
	t_xyz	t3;

	normal = ft_normal(r->plane[i]->dir);
	t3 = ft_diff(r->plane[i]->coor, r->camcoor);
	t1 = ft_dot(&t3, &normal);
	t2 = ft_dot(r->camdir, &normal);
	if (t2 == 0)
		return (0);
	if (t1 / t2 < 0)
		return (0);
	r->t = t1 / t2;
	return (1);
}

int		ft_check_cyl_inter(t_read *r, int i)
{
	t_dcoef	dcoef;
	t_xyz	vec;
	t_xyz	dir_normal;

	dir_normal = ft_normal(r->cyl[i]->dir);
	vec = ft_diff(r->camcoor, r->cyl[i]->coor);
	dcoef.a = ft_dot(r->camdir, r->camdir) -
				pow(ft_dot(r->camdir, &dir_normal), 2);
	dcoef.b = 2 * (ft_dot(r->camdir, &vec) -
		(ft_dot(r->camdir, &dir_normal) * ft_dot(&vec, &dir_normal)));
	dcoef.c = ft_dot(&vec, &vec) -
		pow(ft_dot(&vec, &dir_normal), 2) - pow(r->cyl[i]->r, 2);
	dcoef.d = pow(dcoef.b, 2) - 4 * dcoef.a * dcoef.c;
	if (dcoef.d < 0)
		return (0);
	else
	{
		r->dot1 = (-dcoef.b + sqrt(dcoef.d)) / (2 * dcoef.a);
		r->dot2 = (-dcoef.b - sqrt(dcoef.d)) / (2 * dcoef.a);
		r->t = (r->dot1 <= r->dot2 && r->dot1 > 0) ? r->dot1 : r->dot2;
	}
	return (1);
}

int		ft_check_cone_inter(t_read *r, int i)
{
	t_dcoef	dcoef;
	float	alpha;
	t_xyz	vec;
	t_xyz	dir_normal;

	dir_normal = ft_normal(r->cone[i]->dir);
	alpha = 1 + pow(tan(r->cone[i]->k * M_PI / 180), 2);
	vec = ft_diff(r->camcoor, r->cone[i]->coor);
	dcoef.a = ft_dot(r->camdir, r->camdir) - alpha *
				pow(ft_dot(r->camdir, &dir_normal), 2);
	dcoef.b = 2 * (ft_dot(r->camdir, &vec) - alpha *
		ft_dot(r->camdir, &dir_normal) * ft_dot(&vec, &dir_normal));
	dcoef.c = ft_dot(&vec, &vec) - alpha * pow(ft_dot(&vec, &dir_normal), 2);
	dcoef.d = pow(dcoef.b, 2) - 4 * dcoef.a * dcoef.c;
	if (dcoef.d < 0)
		return (0);
	else
	{
		r->dot1 = (-dcoef.b + sqrt(dcoef.d)) / (2 * dcoef.a);
		r->dot2 = (-dcoef.b - sqrt(dcoef.d)) / (2 * dcoef.a);
		r->t = (r->dot1 <= r->dot2 && r->dot1 > 0) ? r->dot1 : r->dot2;
	}
	return (1);
}
