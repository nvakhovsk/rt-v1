/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_angle.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 15:27:34 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/28 15:33:51 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void	ft_sphere_angle(t_read *r, int i)
{
	t_xyz	normal;
	t_xyz	normal_light;
	float	temp_a;

	normal = ft_diff(&r->vec1, r->ssphere[i]->coor);
	normal = ft_normal(&normal);
	normal_light = ft_diff(r->light, &r->vec1);
	normal_light = ft_normal(&normal_light);
	temp_a = ft_dot(&normal_light, &normal);
	if (r->alpha < temp_a)
		r->alpha = temp_a;
}

void	ft_cylinder_angle_helper(t_read *r, int i, t_xyz *normal)
{
	t_xyz	t;
	t_xyz	dir_normal;
	float	tmp;

	dir_normal = ft_normal(r->cyl[i]->dir);
	t = ft_mult(&dir_normal, r->temp);
	tmp = ft_dot(r->camdir, &t);
	t = ft_diff(r->camcoor, r->cyl[i]->coor);
	tmp = tmp + ft_dot(&t, &dir_normal);
	t = ft_mult(&dir_normal, tmp);
	*normal = ft_diff(&r->vec1, r->cyl[i]->coor);
	*normal = ft_diff(normal, &t);
	*normal = ft_normal(normal);
}

void	ft_cylinder_angle(t_read *r, int i)
{
	t_xyz	normal;
	t_xyz	normal_light;
	float	temp_a;

	ft_cylinder_angle_helper(r, i, &normal);
	normal_light = ft_diff(r->light, &r->vec1);
	normal_light = ft_normal(&normal_light);
	temp_a = ft_dot(&normal_light, &normal);
	if (r->alpha < temp_a)
		r->alpha = temp_a;
}

void	ft_cone_angle_helper(t_read *r, int i, t_xyz *normal)
{
	t_xyz	t;
	t_xyz	dir_normal;
	t_xyz	dot2;
	float	tmp;
	float	k;

	dir_normal = ft_normal(r->cone[i]->dir);
	t = ft_mult(&dir_normal, r->temp);
	tmp = ft_dot(r->camdir, &t);
	t = ft_diff(r->camcoor, r->cone[i]->coor);
	tmp = tmp + ft_dot(&t, &dir_normal);
	k = (1 + pow(tan(r->cone[i]->k * M_PI / 180), 2)) * tmp;
	t = ft_diff(&r->vec1, r->cone[i]->coor);
	dot2 = ft_mult(&dir_normal, k);
	*normal = ft_diff(&t, &dot2);
	*normal = ft_normal(normal);
}

void	ft_cone_angle(t_read *r, int i)
{
	t_xyz	normal;
	t_xyz	normal_light;
	float	temp_a;

	ft_cone_angle_helper(r, i, &normal);
	normal_light = ft_diff(r->light, &r->vec1);
	normal_light = ft_normal(&normal_light);
	temp_a = ft_dot(&normal_light, &normal);
	if (r->alpha < temp_a)
		r->alpha = temp_a;
}
