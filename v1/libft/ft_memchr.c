/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 08:08:31 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/28 16:07:46 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memchr(const void *str, int c, size_t n)
{
	size_t		i;
	const char	*buff;

	i = 0;
	buff = str;
	if ((unsigned char)c == '\0')
	{
		while (buff[i] != '\0' && i < n)
			i++;
		return ((char*)&buff[i]);
	}
	while (buff[i] != '\0' && i < n)
	{
		if (buff[i] == c)
			return ((unsigned char *)&buff[i]);
		i++;
	}
	return (NULL);
}
