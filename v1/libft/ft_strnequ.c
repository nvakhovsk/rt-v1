/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 08:08:31 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/28 14:40:08 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int			ft_strnequ(char const *s1, char const *s2, size_t n)
{
	unsigned int	i;
	unsigned int	ln;
	unsigned char	*str1;
	unsigned char	*str2;

	str1 = (unsigned char*)s1;
	str2 = (unsigned char*)s2;
	ln = (unsigned int)n;
	if ((!(s1)) || (!(s2)))
		return (0);
	i = 0;
	while (str1[i] && i < ln)
	{
		if (str1[i] - str2[i])
			return (0);
		i++;
	}
	if ((str1[i] - str2[i]) && (i != ln))
		return (0);
	return (1);
}
