/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 08:08:31 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/28 16:09:05 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	size_t			i;
	unsigned char	ch_buff;

	i = 0;
	while (n-- > 0)
	{
		ch_buff = ((unsigned char *)src)[i];
		((unsigned char *)dest)[i] = ((unsigned char *)src)[i];
		i++;
		if (ch_buff == (unsigned char)c)
			return (&dest[i]);
	}
	return (NULL);
}
