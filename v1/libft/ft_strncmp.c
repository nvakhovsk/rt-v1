/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 13:07:13 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/28 14:43:22 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	int				res;
	unsigned int	i;

	i = 0;
	res = 0;
	while (s1[i] && !(res) && i < (unsigned int)n)
	{
		res = (int)((unsigned char)s1[i] - (unsigned char)s2[i]);
		i++;
	}
	if (!res && i != n)
		res = (int)((unsigned char)s1[i] - (unsigned char)s2[i]);
	return (res);
}
