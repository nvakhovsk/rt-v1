/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 14:22:35 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/26 15:38:41 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strrchr(const char *str, int c)
{
	unsigned long	i;
	unsigned long	l;

	i = 0;
	l = 0;
	while (str[++i])
		if (str[i] == (char)c)
			l = i;
	if (!c)
		return ((char *)(str + i));
	if (!l)
	{
		if (str[0] == (char)c)
			return ((char *)str);
		else
			return (0);
	}
	return ((char *)(str + l));
}
