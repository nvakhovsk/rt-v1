/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 08:08:31 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/28 16:12:06 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void	*ft_memalloc(size_t size)
{
	void			*str;
	unsigned int	i;

	str = malloc(size);
	if (str == 0)
		return (0);
	i = 0;
	while (i < (unsigned int)size)
	{
		(*((unsigned char *)(str + i))) = 0;
		i++;
	}
	return (str);
}
