/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 09:55:31 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/28 14:24:46 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static unsigned int		ssize(char const *s)
{
	unsigned int	len;
	unsigned int	i;

	i = 0;
	len = 0;
	while ((s[i] == ' ' || s[i] == '\n' || s[i] == '\t') && s[i])
		i++;
	while (s[i + len])
		len++;
	if (!len)
		return (0);
	len--;
	while ((s[i + len] == ' ' || s[i + len] == '\n' || s[i + len] == '\t')
			&& ((len + i) > 0))
		len--;
	return (len + 1);
}

char					*ft_strtrim(char const *s)
{
	unsigned int	j;
	unsigned int	len;
	unsigned int	i;
	char			*str;

	if (!s)
		return (0);
	len = ssize(s);
	if (!(str = (char *)malloc(sizeof(char) * (len + 1))))
		return (0);
	i = 0;
	while ((s[i] == ' ' || s[i] == '\n' || s[i] == '\t') && s[i])
		i++;
	j = 0;
	while (j < len)
	{
		str[j] = s[j + i];
		j++;
	}
	str[j] = 0;
	return (str);
}
