/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 13:28:29 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/26 14:35:25 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(const char *str, const char *find)
{
	int	i;
	int	j;
	int	len;

	len = 0;
	while (find[len] != '\0')
		len++;
	if (!len)
		return ((char *)str);
	i = 0;
	while (str[i + len - 1] != '\0' && str[i] != '\0')
	{
		if (str[i] == *find)
		{
			j = 0;
			while (str[i + j] == find[j] && str[i + j])
				j++;
			if (j == len)
				return ((char*)&str[i]);
		}
		i++;
	}
	return (0);
}
